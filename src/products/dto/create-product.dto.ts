import { IsNotEmpty, Length, IsInt } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  @IsInt()
  price: number;
}
